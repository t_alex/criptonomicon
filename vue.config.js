module.exports = {
  parallel: false,
  chainWebpack: config => {
    config.module
      .rule("worker")
      .test(/_worker\.js$/)
      .use("worker-loader")
      .loader("worker-loader")
      // eslint-disable-next-line no-unused-vars
      .tap(options => ({
        worker: "SharedWorker",
        filename: "[name].41.js"
      }))
      .end();
  }
};
