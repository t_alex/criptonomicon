import { setTimeout } from "core-js";
import Worker from "./shared_worker";
const worker = new Worker();

// идентификатор для определения источника сообщения
const id =
  Date.now().toString(36) +
  Math.random()
    .toString(36)
    .substring(2);

// eslint-disable-next-line no-unused-vars
let webSocketState = WebSocket.CONNECTING;
console.log(`init worker for ${id}`);
worker.port.start();
worker.onmessage = event => {
  switch (event.data.type) {
    case "WSState":
      webSocketState = event.data.state;
      break;
    case "message":
      // можно поставить фильтр на источник сообщения event.data.from === id
      // сейчас принимаем все
      onmessage(event.data);
      break;
  }
};

const bChannel = new BroadcastChannel("wsChannel");
bChannel.addEventListener("message", event => {
  switch (event.data.type) {
    case "WSState":
      webSocketState = event.data.state;
      break;
    case "message":
      onmessage(event.data);
      break;
  }
});

const API_KEY =
  "8f520a23a8485262a20df6cf7f3f73267552310c766aaa71a3a6067e6485752f";

const MESSAGE_TYPE = {
  AG_INDEX: "5",
  INVALID_SUB: "500"
};

const BASE_QUOTE = "USD";
const BASE_COIN = "BTC";

const tickerHendlers = new Map();
const subsCache = new Map(); // храним подписки для моент, чтобы определить не существующую
const rates = new Map(); // курсы монет
let msgCache = [];

function sendToWebsocket(message) {
  if (webSocketState === WebSocket.CONNECTING) {
    if (msgCache.find(el => el === message)) {
      console.log("ws error, not connected", message);
      return;
    } else {
      console.log("ws connecting", message);
    }
    msgCache.push(message);
    // ожидаем открытия сокета через 5сек.
    setTimeout(() => {
      sendToWebsocket(message);
    }, 5000);
  } else if (
    webSocketState === WebSocket.CLOSING ||
    webSocketState === WebSocket.CLOSED
  ) {
    console.log("ws close");
  } else if (webSocketState == WebSocket.OPEN) {
    // отправляем сообщение
    worker.port.postMessage({
      from: id,
      data: message
    });
  }
}

function onmessage({ data }) {
  const {
    TYPE: type,
    FROMSYMBOL: currency,
    TOSYMBOL: quote,
    PRICE: newPrice,
    MESSAGE: message,
    PARAMETER: subName
  } = data;

  if (type === MESSAGE_TYPE.INVALID_SUB && message === "INVALID_SUB") {
    // посмотрим кеш подписок
    subsCache.forEach((subs, tickerName) => {
      if (subs.find(el => el === subName)) {
        const newSubs = [
          `5~CCCAGG~${tickerName}~${BASE_COIN}`,
          `5~CCCAGG~${BASE_COIN}~${BASE_QUOTE}`
        ];
        // если подписки добавлялись ранее, тогда монеты не существует
        newSubs.forEach(sub => {
          if (subs.find(el => el === sub)) {
            // выводим ошибку
            const handlers = tickerHendlers.get(tickerName) ?? [];
            handlers.forEach(fn => fn("-", true));
            return;
          }
          // обновим кеш
          subs.push(sub);
        });
        // добавим подписки
        sendToWebsocket({
          action: "SubAdd",
          subs: newSubs
        });
      }
    });
  }

  if (type === MESSAGE_TYPE.AG_INDEX && newPrice != undefined) {
    // валюту добавляем в курсы
    if (currency != BASE_COIN && quote === BASE_COIN) {
      rates.set(currency, newPrice);
    }

    // валюта из подписки
    const handlers = tickerHendlers.get(currency) ?? [];
    handlers.forEach(fn => fn(newPrice, false));

    if (currency === BASE_COIN) {
      // получим курсы зависимых валют и обновим их
      rates.forEach((rate, currency) => {
        const handlers = tickerHendlers.get(currency) ?? [];
        handlers.forEach(fn => fn(newPrice * rate, false));
      });
    }
  }
}

function subscribeToTickerWS(tickerName) {
  const subName = `5~CCCAGG~${tickerName}~${BASE_QUOTE}`;
  const subs = subsCache.get(tickerName) ?? [];

  if (!subs.find(el => el === subName)) {
    subsCache.set(tickerName, [...subs, subName]);
  }

  sendToWebsocket({
    action: "SubAdd",
    subs: [subName]
  });
}

function unsubscribeFromTickerWS(tickerName) {
  const subName = `5~CCCAGG~${tickerName}~${BASE_QUOTE}`;
  const subs = subsCache.get(tickerName) ?? [];

  subsCache.delete(tickerName);

  sendToWebsocket({
    action: "SubRemove",
    subs: [...subs, subName]
  });
}

export const subscribeToTicker = (tickerName, cb) => {
  const handlers = tickerHendlers.get(tickerName) ?? [];
  tickerHendlers.set(tickerName, [...handlers, cb]);
  subscribeToTickerWS(tickerName);
};

export const unsubscribeFromTicker = tickerName => {
  tickerHendlers.delete(tickerName);
  unsubscribeFromTickerWS(tickerName);
};

export const loadCoins = () =>
  fetch(
    `https://min-api.cryptocompare.com/data/all/coinlist?summary=true&api_key=${API_KEY}`
  ).then(res => res.json());
