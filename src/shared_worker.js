const API_KEY =
  "8f520a23a8485262a20df6cf7f3f73267552310c766aaa71a3a6067e6485752f";

const ws = new WebSocket(
  `wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`
);

const idToPortMap = {};

// будет следить за состоянием сокета
const bChannel = new BroadcastChannel("wsChannel");
bChannel.onmessage = ({ message }) => {
  sendToWebsocket(message);
};

ws.onopen = () => {
  bChannel.postMessage({
    type: "WSState",
    state: ws.readyState
  });
};
ws.onclose = () => {
  bChannel.postMessage({
    type: "WSState",
    state: ws.readyState
  });
};

ws.onmessage = ({ data }) => {
  const parsedData = {
    data: JSON.parse(data),
    type: "message"
  };
  if (!parsedData.data.from) {
    bChannel.postMessage(parsedData);
  } else {
    idToPortMap[parsedData.data.from].postMessage(parsedData);
  }
};

function sendToWebsocket(message) {
  const textMessage = JSON.stringify(message.data);
  if (ws.readyState == WebSocket.OPEN) {
    ws.send(textMessage);
    return;
  }

  ws.addEventListener("open", () => {
    ws.send(textMessage);
  });
}

// eslint-disable-next-line no-undef
onconnect = e => {
  const port = e.ports[0];
  port.onmessage = msg => {
    idToPortMap[msg.data.from] = port;
    sendToWebsocket(msg.data);
  };
  // из воркера тоже обновляем состояние сокета
  port.postMessage({
    state: ws.readyState,
    type: "WSState"
  });
};
